<?php

namespace spec\Katas;

use Katas\FizzBuzz;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FizzBuzzSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(FizzBuzz::class);
    }

    function it_returns_1_for_1()
    {
        $this->transform(1)->shouldReturn(1);
    }

    function it_returns_2_for_2()
    {
        $this->transform(2)->shouldReturn(2);
    }

    function it_returns_1_2_for_1_2()
    {
        $this->transform([1, 2])->shouldReturn([1, 2]);
    }

    function it_returns_fizz_if_divisible_by_3()
    {
        $this->transform(3)->shouldReturn('fizz');
    }

    function it_returns_buzz_if_divisible_by_5()
    {
        $this->transform(5)->shouldReturn('buzz');
    }

    function it_returns_fizz_for_6()
    {
        $this->transform(6)->shouldReturn('fizz');
    }

    function it_returns_fizzbuzz_if_divisible_by_3_and_5()
    {
        $this->transform(15)->shouldReturn('fizzbuzz');
    }

    function it_translates_a_given_input()
    {
        $this->transform([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
            ->shouldReturn([1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz', 11, 'fizz', 13, 14, 'fizzbuzz']);
    }
}
