<?php

namespace spec\Katas;

use Katas\RomanNumeralConverter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RomanNumeralConverterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RomanNumeralConverter::class);
    }

    function it_returns_I_for_1()
    {
        $this->convert(1)->shouldBeEqualTo('I');
    }

    function it_returns_II_for_2()
    {
        $this->convert(2)->shouldBeEqualTo('II');
    }

    function it_returns_III_for_3()
    {
        $this->convert(3)->shouldBeEqualTo('III');
    }

    function it_returns_IV_for_4()
    {
        $this->convert(4)->shouldBeEqualTo('IV');
    }

    function it_returns_V_for_5()
    {
        $this->convert(5)->shouldBeEqualTo('V');
    }

    function it_returns_VI_for_6()
    {
        $this->convert(6)->shouldBeEqualTo('VI');
    }

    function it_returns_VII_for_7()
    {
        $this->convert(7)->shouldBeEqualTo('VII');
    }

    function it_returns_VIII_for_8()
    {
        $this->convert(8)->shouldBeEqualTo('VIII');
    }

    function it_returns_IX_for_9()
    {
        $this->convert(9)->shouldBeEqualTo('IX');
    }

    function it_returns_X_for_10()
    {
        $this->convert(10)->shouldBeEqualTo('X');
    }

    function it_returns_XI_for_11()
    {
        $this->convert(11)->shouldBeEqualTo('XI');
    }

    function it_returns_XII_for_12()
    {
        $this->convert(12)->shouldBeEqualTo('XII');
    }

    function it_returns_XIII_for_13()
    {
        $this->convert(13)->shouldBeEqualTo('XIII');
    }

    function it_returns_XV_for_15()
    {
        $this->convert(15)->shouldBeEqualTo('XV');
    }

    function it_returns_XX_for_20()
    {
        $this->convert(20)->shouldBeEqualTo('XX');
    }

    function it_returns_XXX_for_30()
    {
        $this->convert(30)->shouldBeEqualTo('XXX');
    }

    function it_returns_XL_for_40()
    {
        $this->convert(40)->shouldBeEqualTo('XL');
    }

    function it_returns_L_for_50()
    {
        $this->convert(50)->shouldBeEqualTo('L');
    }

    function it_returns_LX_for_60()
    {
        $this->convert(60)->shouldBeEqualTo('LX');
    }

    function it_returns_XC_for_90()
    {
        $this->convert(90)->shouldBeEqualTo('XC');
    }

    function it_returns_C_for_100()
    {
        $this->convert(100)->shouldBeEqualTo('C');
    }

    function it_returns_CC_for_200()
    {
        $this->convert(200)->shouldBeEqualTo('CC');
    }

    function it_returns_D_for_500()
    {
        $this->convert(500)->shouldBeEqualTo('D');
    }

    function it_returns_CM_for_900()
    {
        $this->convert(900)->shouldBeEqualTo('CM');
    }

    function it_returns_CMXCIV_for_994()
    {
        $this->convert(994)->shouldBeEqualTo('CMXCIV');
    }

    function it_returns_CMXCIX_for_999()
    {
        $this->convert(999)->shouldBeEqualTo('CMXCIX');
    }

    function it_returns_M_for_1000()
    {
        $this->convert(1000)->shouldBeEqualTo('M');
    }

    function it_returns_MCMXCIX_for_1999()
    {
        $this->convert(1999)->shouldBeEqualTo('MCMXCIX');
    }

    function it_returns_MM_for_2000()
    {
        $this->convert(2000)->shouldBeEqualTo('MM');
    }

    function it_returns_MMXVI_for_2016()
    {
        $this->convert(2016)->shouldBeEqualTo('MMXVI');
    }

    function it_returns_MMMCDLXXV_for_3475()
    {
        $this->convert(3475)->shouldBeEqualTo('MMMCDLXXV');
    }

    function it_returns_MMCMXCIV_for_2994()
    {
        $this->convert(2994)->shouldBeEqualTo('MMCMXCIV');
    }

    function it_returns_MMMMCMXC_for_4990()
    {
        $this->convert(4990)->shouldBeEqualTo('MMMMCMXC');
    }
}
