<?php

namespace spec\Katas;

use Katas\StringCalculator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StringCalculatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(StringCalculator::class);
    }

    function it_translate_and_empty_string_to_zero()
    {
        $this->sum('')->shouldEqual(0);
    }

    function it_translate_a_1_for_1()
    {
        $this->sum('1')->shouldEqual(1);
    }

    function it_translate_a_string_number_to_integer()
    {
        $number = rand(10, 50);
        $this->sum("{$number}")->shouldEqual($number);
    }

    function it_sum_2_number_separated_by_coma()
    {
        $this->sum('4,6')->shouldEqual(10);
    }

    function it_sum_4_number_separated_by_coma()
    {
        $this->sum('4,6,5,7')->shouldEqual(22);
    }

    function it_sum_2_number_separated_by_new_line()
    {
        $this->sum('4\n6')->shouldEqual(10);
    }

    public function it_shouldnt_sum_strings_equal_or_bigger_than_1000()
    {
        $this->sum('1000')->shouldEqual(0);
    }

    function it_return_14_for_7_4_1100_3()
    {
        $this->sum('7,4,1100,3')->shouldEqual(14);
    }

    function it_translate_a_string()
    {
        $this->sum('7\n15\n3,5,1100,3')->shouldEqual(33);
    }

    function it_disallows_negative_numbers()
    {
        $number = '-3';
        $this->shouldThrow(new \InvalidArgumentException('Invalid number provided: ' . $number))
            ->duringSum($number);
    }
}
