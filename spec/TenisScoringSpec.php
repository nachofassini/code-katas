<?php

namespace spec\Katas;

use Katas\TenisScoring;
use Katas\Player;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TenisScoringSpec extends ObjectBehavior
{
    protected $jugador1;
    protected $jugador2;

    function let()
    {
        $this->jugador1 = new Player('Juan', 0);
        $this->jugador2 = new Player('Jose', 0);
        $this->beConstructedWith($this->jugador1, $this->jugador2);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TenisScoring::class);
    }

    function it_scores_a_scoreless_game()
    {
        $this->score()->shouldReturn('Love-All');
    }

    function it_scores_a_1_0_game()
    {
        $this->jugador1->earnPoints(1);
        $this->score()->shouldReturn('Fifteen-Love');
    }

    function it_scores_a_2_0_game()
    {
        $this->jugador1->earnPoints(2);
        $this->score()->shouldReturn('Thirty-Love');
    }

    function it_scores_a_3_0_game()
    {
        $this->jugador1->earnPoints(3);
        $this->score()->shouldReturn('Forty-Love');
    }

    function it_scores_a_1_1_game()
    {
        $this->jugador1->earnPoints(1);
        $this->jugador2->earnPoints(1);
        $this->score()->shouldReturn('Fifteen-All');
    }

    function it_scores_a_3_3_game()
    {
        $this->jugador1->earnPoints(3);
        $this->jugador2->earnPoints(3);
        $this->score()->shouldReturn('Forty-All');
    }

    function it_scores_a_4_4_game()
    {
        $this->jugador1->earnPoints(4);
        $this->jugador2->earnPoints(4);
        $this->score()->shouldReturn('Deuce');
    }

    function it_scores_a_5_5_game()
    {
        $this->jugador1->earnPoints(5);
        $this->jugador2->earnPoints(5);
        $this->score()->shouldReturn('Deuce');
    }

    function it_scores_a_4_2_game()
    {
        $this->jugador1->earnPoints(4);
        $this->jugador2->earnPoints(2);
        $this->score()->shouldReturn('Game for: Juan');
    }

    function it_scores_a_2_4_game()
    {
        $this->jugador1->earnPoints(2);
        $this->jugador2->earnPoints(4);
        $this->score()->shouldReturn('Game for: Jose');
    }

    function it_scores_a_4_3_game()
    {
        $this->jugador1->earnPoints(4);
        $this->jugador2->earnPoints(3);
        $this->score()->shouldReturn('Advantage Juan');
    }

    function it_scores_a_3_4_game()
    {
        $this->jugador1->earnPoints(3);
        $this->jugador2->earnPoints(4);
        $this->score()->shouldReturn('Advantage Jose');
    }

    function it_scores_a_5_3_game()
    {
        $this->jugador1->earnPoints(5);
        $this->jugador2->earnPoints(3);
        $this->score()->shouldReturn('Game for: Juan');
    }

    function it_scores_a_3_5_game()
    {
        $this->jugador1->earnPoints(3);
        $this->jugador2->earnPoints(5);
        $this->score()->shouldReturn('Game for: Jose');
    }
}
