<?php
namespace Katas;

class BowlingGame
{
    protected $score = 0;
    protected $rolls = [];

    public function roll($pins)
    {
        $this->validateRoll($pins);

        $this->rolls[] = $pins;
    }

    public function score()
    {
        $roll = 0;

        for ($frame = 1; $frame <= 10; $frame++) {
            if ($this->isStrike($roll)) {
                $score = 10 + $this->getStrikeBonus($roll);
                $roll = $roll + 1;
            } elseif ($this->isSpare($roll)) {
                $score = 10 + $this->getSpareBonus($roll);
                $roll = $roll + 2;
            } else {
                $score = $this->getDefaulFrameScore($roll);
                $roll = $roll + 2;
            }

            $this->score += $score;
        }

        return $this->score;
    }

    /**
     * @param $roll
     * @return bool
     */
    public function isSpare($roll): bool
    {
        return $this->rolls[$roll] + $this->rolls[$roll + 1] == 10;
    }

    /**
     * @param $roll
     * @return bool
     */
    public function isStrike($roll): bool
    {
        return $this->rolls[$roll] == 10;
    }

    /**
     * @param $roll
     * @return mixed
     */
    private function getDefaulFrameScore($roll)
    {
        return $this->rolls[$roll] + $this->rolls[$roll + 1];
    }

    /**
     * @param $roll
     * @return mixed
     */
    private function getStrikeBonus($roll): int
    {
        return $this->rolls[$roll + 1] + $this->rolls[$roll + 2];
    }

    /**
     * @param $roll
     * @return mixed
     */
    private function getSpareBonus($roll): int
    {
        return $this->rolls[$roll + 2];
    }

    /**
     * @param $pins
     */
    public function validateRoll($pins)
    {
        if ($pins < 0 || $pins > 10) {
            throw new \InvalidArgumentException('Invalid Roll');
        }
    }
}
