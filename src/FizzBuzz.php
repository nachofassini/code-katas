<?php

namespace Katas;

class FizzBuzz
{
    public function transform($input)
    {
        if (is_array($input)) {
            return array_map(function ($item) {
                return $this->translate($item);
            }, $input);
        }

        return $this->translate($input);
    }

    public function translate(int $number)
    {
        if ($number % 15 == 0) return 'fizzbuzz';
        if ($number % 5 == 0) return 'buzz';
        if ($number % 3 == 0) return 'fizz';

        return $number;
    }
}
