<?php

namespace Katas;

class Player {
    public $name;
    public $points;

    /**
     * Player constructor.
     * @param string $name
     * @param int $points
     */
    public function __construct($name, $points)
    {
        $this->name = $name;
        $this->points = $points;
    }

    public function earnPoints(int $points)
    {
        $this->points += $points;
        return $this;
    }

}
