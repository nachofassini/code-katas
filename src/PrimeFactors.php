<?php
namespace Katas;

class PrimeFactors
{
    public function generate(int $number = null): array
    {
        $primes = [];

        for ($divider = 2; $number > 1; $divider++) {
            for (;$number % $divider == 0; $number /= $divider) {
                $primes[] = $divider;
            }
        }

        return $primes;
    }
}
