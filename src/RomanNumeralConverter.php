<?php
namespace Katas;

class RomanNumeralConverter
{
    protected static $lookup = [
        1000 => 'M',
        900 => 'CM',
        500 => 'D',
        400 => 'CD',
        100 => 'C',
        90 => 'XC',
        50 => 'L',
        40 => 'XL',
        10 => 'X',
        9 => 'IX',
        5 => 'V',
        4 => 'IV',
        1 => 'I',
    ];

    public function convert(int $number): string
    {
        $solution = '';

        foreach (static::$lookup as $limit => $gliph) {
            while ($number >= $limit) {
                $solution .= $gliph;
                $number -= $limit;
            }
        }

        return $solution;
    }
}
