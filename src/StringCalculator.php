<?php
namespace Katas;

class StringCalculator
{
    const MAX_NUMBER_ALLOWED = 1000;
    protected $numbers = [];

    public function sum(string $input): int
    {
        $this->parseInput($input)->clean();
        return (int)array_sum($this->numbers);
    }

    private function parseInput(string $input)
    {
        $this->numbers = preg_split('/\s*(,|\\\n)\s*/', $input);
        return $this;
    }

    private function clean()
    {
        $this->numbers = array_filter($this->numbers, function ($number) {
            if ($number >= self::MAX_NUMBER_ALLOWED) {
                return false;
            }

            $this->guardAgainstInvalidNumbers($number);
            return true;
        });
        return $this;
    }

    /**
     * @param $number
     */
    function guardAgainstInvalidNumbers($number)
    {
        if ($number < 0) {
            throw new \InvalidArgumentException('Invalid number provided: ' . $number);
        }
    }
}
