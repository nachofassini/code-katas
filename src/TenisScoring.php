<?php

namespace Katas;

class TenisScoring
{
    protected $jugador1;
    protected $jugador2;
    protected $lookup = [
        0 => 'Love',
        1 => 'Fifteen',
        2 => 'Thirty',
        3 => 'Forty',
    ];

    /**
     * TenisScoring constructor.
     * @param $jugador1
     * @param $jugador2
     */
    public function __construct(Player $jugador1, Player $jugador2)
    {
        $this->jugador1 = $jugador1;
        $this->jugador2 = $jugador2;
    }


    public function score()
    {
        if ($this->gameTied()) {
            if ($this->jugador1->points < 4)
                return "{$this->lookup[$this->jugador1->points]}-All";

            if ($this->jugador1->points >= 4)
                return 'Deuce';
        }

        if ($this->hasWinner()) {
            $winner = $this->getLeader();
            return "Game for: {$winner->name}";
        }

        if ($this->hasAdvantage()) {
            $leader = $this->getLeader();
            return "Advantage {$leader->name}";
        }

        return $this->getGeneralScore();
    }

    /**
     * @return bool
     */
    private function gameTied(): bool
    {
        return $this->jugador1->points == $this->jugador2->points;
    }

    /**
     * @return bool
     */
    private function hasWinner(): bool
    {
        return $this->playersHasEnoughPointsToBeWon() && $this->playerIsLeadingBy(2);
    }

    /**
     * @return bool
     */
    private function hasAdvantage(): bool
    {
        return $this->playersHasEnoughPointsToBeWon() && $this->playerIsLeadingBy(1);
    }

    private function getLeader()
    {
        if ($this->jugador1->points >= 4) {
            return $this->jugador1;
        }
        return $this->jugador2;
    }

    /**
     * @return bool
     */
    private function playersHasEnoughPointsToBeWon(): bool
    {
        return max($this->jugador1->points, $this->jugador2->points) >= 4;
    }

    /**
     * @return bool
     */
    private function playerIsLeadingBy(int $points): bool
    {
        return abs($this->jugador1->points - $this->jugador2->points) >= $points;
    }

    /**
     * @return string
     */
    private function getGeneralScore(): string
    {
        return "{$this->lookup[$this->jugador1->points]}-{$this->lookup[$this->jugador2->points]}";
    }
}
